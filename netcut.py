#!/usr/bin/env python3.6
# Currently no more recent python version than 3.6
# can run netfilterqueue

# This program is most useful at a MITM scenario
# then you might want to execute this command before
# using this program:
# iptables -I FORWARD -j NFQUEUE --queue-num 0
# NB: FORWARD is a chain that contains packets than
# are intended for other machines than the one running
# this program.
# Make sure forwarding has been enabled:
# echo 1 > /proc/sys/net/ipv4/ip_forward

# If you intend to monitor this machine's packets,
# you may want to execute these commands before using
# this program:
# iptables -I INPUT -j NFQUEUE --queue-num 0
# iptables -I OUTPUT -j NFQUEUE --queue-num 0
# NB: INPUT and OUTPUT are chains which contain packet
# that comes to and goes from this machine

import netfilterqueue as nfq
import scapy.all as scapy

def process_packet(packet):
    print(packet)
    packet.drop()

queue = nfq.NetfilterQueue()
queue.bind(0, process_packet)
queue.run()
